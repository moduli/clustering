from core.model.tools.id_generator import IDGenerator, area_from_fid
from core.cluster.elasticsearch.client import ElasticSearchClient
from core.cluster.graph.algorithm import Clustering
from dependencies.elasticsearch_5_4_0.helpers import scan
from elasticsearch.client.utils import NamespacedClient, query_params, _make_path
from elasticsearch.exceptions import TransportError, RequestError

from collections import defaultdict
from datetime import datetime
import logging
from django.conf import settings

logger = logging.getLogger(__name__)
use_dynamic = getattr(settings, 'USE_DYNAMIC', True)
load_raw = getattr(settings, 'LOAD_RAW', False)


class GraphClient(object):
    def __init__(self):
        self.client = ElasticSearchClient.get_write_client()
        self.indices_client = ElasticSearchClient.get_indices_client()
        self.namespace_client = NamespacedClient(self.client)

        self.read_client = ElasticSearchClient.get_read_client()
        self.index = None

        self.infringement_doctype = None
        self.quantity_metric = None
        self.filters = []
        self.terms = None
        self.search_term = ""
        self.sample_size = 1000000

        self.raw_documents_index = self.get_raw_documents_index()
        self.cluster_index = self.get_cluster_index()
        self.cluster_most_recent = self.get_cluster_most_recent_doctype()
        self.cluster_timeline = self.get_cluster_timeline_doctype()

    @query_params('routing', 'timeout')
    def explore(self, client_id, body=None, params=None):
        """
        Graph Explore Feature
        <https://www.elastic.co/guide/en/elasticsearch/reference/current/graph-explore-api.html>

        :param client_id
        :param body: Graph Query DSL
        :param params:
        :return:
        """
        if client_id:
            _path = _make_path(client_id, self.infringement_doctype, '_xpack', '_graph', '_explore')
        else:
            _path = "/" + _make_path(client_id, self.infringement_doctype, '_xpack', '_graph', '_explore')

        return self.namespace_client.transport.perform_request('GET', _path, params=params, body=body)

    @staticmethod
    def get_cluster_index():
        return u"ui_clusters"

    @staticmethod
    def get_raw_documents_index():
        return u"raw_documents"

    @staticmethod
    def get_cluster_most_recent_doctype():
        return u"most_recent"

    @staticmethod
    def get_cluster_timeline_doctype():
        return u"timeline"

    def get_index_size(self):
        """
        Get index size and use as sample size for graph query
        :return:
        """
        hits = self.client.search(index=self.index)
        if hits:
            count = hits['hits']['total']
            return count

    def get_filters(self, *args, **kwargs):
        """
        Convert user defined filter dictionary to Query DSL format
        E.g. {
            "areas": ["marketplace_infringement", "socialmedia_infringement"],
            "current_status": "offfline",
            "confidence": {"gte": 0.5, "lte": 1.5}
        }
        :param args:
        :param kwargs:
        :return:
        """
        self.filters = []

        if kwargs:
            for key, value in kwargs.items():

                if isinstance(value, (str, unicode, int, float, long)):
                    if value == "":
                        pass
                    else:
                        self.filters.append({"term": {key: value}})
                elif isinstance(value, dict):
                    self.filters.append({"range": {key: value}})

                elif isinstance(value, list):
                    if value == []:
                        pass
                    else:
                        self.filters.append({"terms": {key: value}})
                else:
                    pass

            return self.filters

    def get_terms(self, *args, **kwargs):
        """
        Convert user defined input to Query DSL format
        E.g. "+8615011858714"
        :param args:
        :param kwargs:
        :return:
        """

        self.terms = []

        if args:
            for value in args:

                if isinstance(value, (str, unicode, int, float)):
                        self.filters.append({"term": {value}})
                else:
                    pass

            return self.terms

    def get_query_framework(self):
        """
        Query Profile will use base query to build out
        :return:
        """
        query = {
            "query": {
                "bool": {
                    "must": {
                        "query_string": {
                            "query": self.search_term
                        }
                    },
                    "filter": self.filters
                }
            },
            "controls": {
                "use_significance": True,
                "sample_size": self.sample_size,
                "timeout": 20000
            },
            "connections": {
                "vertices": []
            },
            "vertices": []
        }

        return query

    def get_base_query(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        query = self.get_query_framework()
        list_of_vertices = []

        if kwargs:
            for key, value in kwargs.items():
                list_of_vertices.append(
                    {
                        "field": key,
                        "size": int(value),
                        "min_doc_count": 1
                    }
                )

            query['vertices'] = list_of_vertices
            query['connections']['vertices'] = list_of_vertices

            return query
        else:
            pass

    def get_most_recent_cluster(self, cluster):
        """

        :param cluster:
        :return:
        """
        if cluster:
            if isinstance(cluster, dict):
                if '_source' in cluster.keys():
                    try:
                        keys = []

                        for key in cluster['_source'].keys():
                            try:
                                # Check if key is of intended format
                                keys.append(datetime.strptime(key, "%Y-%m-%d %H:%M:%S"))
                            except:
                                pass

                        if keys:
                            keys = self.get_most_recent_cluster_key(keys)
                            return {"cluster_date": keys, "_source": cluster['_source'][keys]}

                        else:
                            logger.info('No keys could be found')
                            print("No keys could be found.")

                    except Exception as e:
                        logger.error('Exception `%s` getting most recent cluster', e, exc_info=True)
                        print(e)

    def get_most_recent_cluster_key(self, keys):
        """
        Get a list of datetime objects and return the most recent object
        :param keys:
        :return:
        """
        if keys:
            if isinstance(keys, list):
                keys = max(keys)

                if isinstance(keys, datetime):
                    keys = datetime.strftime(keys, "%Y-%m-%d %H:%M:%S")
                    return keys

    @staticmethod
    def get_datetime_key():
        """
        Get current datetime to be used as key in cluster element
        :return:
        """
        datetime_key = datetime.utcnow()
        datetime_key = datetime_key.strftime("%Y-%m-%d %H:%M:%S")
        return datetime_key

    def get_cluster_id_for_query(self, client_id, query_string, search_depth=5):
        """
        search for an existing query for the same starting point - if found return the
        cluster id - otherwise return None as no results have every been saved
        """
        entity_type, entity_value = self._extract_entity_from_query(query_string)
        query = {
            'query': {
                'bool': {
                    "must": [
                        {
                            'term': {
                                'client_id': client_id
                            }
                        },
                        {
                            'term': {
                                'query_type': '{}'.format(entity_type)
                            }
                        },
                        {
                            'term': {
                                'query_value': '"{}"'.format(entity_value)
                            }
                        }
                    ],
                }
            },
        }
        r = self.client.search(
            index=self.cluster_index,
            doc_type=self.cluster_most_recent,
            body=query,
            size=3,
        )
        if r['hits']['total']:
            return r['hits']['hits'][0]['_source']['cluster_id']
        return None

    def get_clustered_connections(self, response, connections):
        """

        :param response:
        :param connections:
        :return:
        """
        if "connections" in response.keys():
            if response["connections"]:
                for element in response["connections"]:

                    source_term = response["vertices"][element["source"]]["term"]
                    target_term = response["vertices"][element["target"]]["term"]

                    element["source"] = source_term
                    element["target"] = target_term

                    if not source_term == target_term:
                        connections.append(element)

        return connections

    def get_root_node(self, response, query_string):
        """

        :param response:
        :param query_string:
        :return:
        """
        found_root_node = False
        entity_type, entity_value = self._extract_entity_from_query(query_string)

        if response:
            if "vertices" in response.keys():
                if response["vertices"]:
                    for element in response["vertices"]:

                        if element["field"] == u"user_id":
                            element["field"] = u"user"

                        if element["term"] == entity_value:
                            found_root_node = True
                            element["root_node"] = True
                        else:
                            element["root_node"] = False

                if found_root_node:
                    pass
                else:

                    if entity_type == u"user_id":
                        entity_type = u"user"

                    response["vertices"].append(
                        {
                            u"field": entity_type,
                            u"term": entity_value,
                            u"depth": 0,
                            u"weight": 1.0,
                            u"root_node": True
                        }
                    )

        return response

    def get_root_entities(self, client_id, query_string):
        """

        :param query_string:
        :return:
        """
        try:
            root_entities = []

            query = {
                "query": {
                    "query_string": {
                        "query": query_string
                    }
                },
                "size": 10000
            }

            raw_documents = self.client.search(
                index=client_id,
                body=query
            )

            # raw_documents = scan(self.client, query=query, scroll='1m', index=client_id)

            columns = Clustering.get_clustering_columns()

            for doc in raw_documents['hits']['hits']:
                if doc:
                    for key in columns.keys():
                        if key in doc["_source"].keys():
                            if doc["_source"][key]:
                                if isinstance(doc["_source"][key], list):
                                    for element in doc["_source"][key]:
                                        if element not in root_entities:
                                            root_entities.append(element)
                                else:
                                    if doc["_source"][key] not in root_entities:
                                        root_entities.append(doc["_source"][key])

            root_entities = list(set(root_entities))
            return root_entities

        except Exception as e:
            logger.error('Exception `%s` getting root entities', e, exc_info=True)
            print(e)

    def get_root_node_connections(self, response, root_entities, connections_vertices, query_string):
        """

        :return:
        """
        entity_type, entity_value = self._extract_entity_from_query(query_string)

        for entity in response['vertices']:
            if entity["term"] in root_entities and entity['root_node'] is not True:

                response["connections"].append({
                    u"source": connections_vertices[entity_value],
                    u"target": connections_vertices[entity["term"]],
                    u"weight": 1,
                    u"doc_count": 1
                })

        return response

    def get_stripped_nodes_and_depth(self, response):
        """

        :return:
        """
        for key, value in enumerate(response['vertices']):
            if value["root_node"]:
                root_node = key
            else:
                value["depth"] = None

        root_node

        entities_list = [root_node]

        for i in range(1, 20):
            sub_entity_list = []

            for element in response["connections"]:
                if element["source"] in entities_list:
                    if response["vertices"][element["target"]][u"depth"] is not None:
                        pass
                    else:
                        response["vertices"][element["target"]][u"depth"] = i
                        sub_entity_list.append(element["target"])

                if element["target"] in entities_list:
                    if response["vertices"][element["source"]][u"depth"] is not None:
                        pass
                    else:
                        response["vertices"][element["source"]][u"depth"] = i
                        sub_entity_list.append(element["source"])

            for elem in sub_entity_list:
                entities_list.append(elem)

            entities_list = list(set(entities_list))

        return response

    def get_cluster_response(self, client_id, query_string):
        """

        :param client_id
        :param query_string:
        :return:
        """
        connections = []
        connections_vertices = {}

        root_entities = self.get_root_entities(client_id, query_string)

        query = Clustering.get_initial_query(Clustering(), query_string)
        response = self.explore(client_id=client_id, body=query)

        connections = self.get_clustered_connections(response, connections)

        # Check to see if object exists and can be spidered on. Else return
        if response["connections"] and response["vertices"]:
            spidered = True
            max_num_iterations = 0

            while spidered:
                entities_list = self.extract_entities_list_from_response(response)

                query = Clustering.get_spidering_query(Clustering(), entities_list)

                response = self.explore(client_id=client_id, body=query)

                connections = self.get_clustered_connections(response, connections)

                max_num_iterations += 1

                # Limited to ensure cluster size isn't excessive.
                # if not response["connections"] or max_num_iterations >= 5 or len(response['vertices']) >= 1024:
                #     spidered = False

                # Potentially generate much bigger clusters
                if not response["connections"] or max_num_iterations >= 25:
                    spidered = False

        response = self.get_root_node(response, query_string)

        for key, value in enumerate(response["vertices"]):
            connections_vertices[value["term"]] = key

        for connection in connections:
            connection["source"] = connections_vertices[connection["source"]]
            connection["target"] = connections_vertices[connection["target"]]

        response["connections"] = connections

        response = self.get_root_node_connections(response, root_entities, connections_vertices, query_string)

        response = self.get_stripped_nodes_and_depth(response)

        return response

    def get_dynamic_cluster_response(self, client_id, query_string):
        """

        :param client_id
        :param query_string:
        :return:
        """
        connections = []
        connections_vertices = {}

        root_entities = self.get_root_entities(client_id, query_string)

        query = Clustering.get_initial_query(Clustering(), query_string)
        response = self.explore(client_id=client_id, body=query)

        connections = self.get_clustered_connections(response, connections)

        # Check to see if object exists and can be spidered on. Else return
        if response["connections"] and response["vertices"]:
            spidered = True
            max_num_iterations = 0

            while spidered:
                entities_list = self.extract_entities_list_from_response(response)

                query = Clustering.get_spidering_query(Clustering(), entities_list)

                response = self.explore(client_id=client_id, body=query)

                connections = self.get_clustered_connections(response, connections)

                max_num_iterations += 1

                # Limited to ensure cluster size isn't excessive.
                if not response["connections"] or max_num_iterations >= 5 or len(response['vertices']) >= 1024:
                    spidered = False

        response = self.get_root_node(response, query_string)

        for key, value in enumerate(response["vertices"]):
            connections_vertices[value["term"]] = key

        for connection in connections:
            connection["source"] = connections_vertices[connection["source"]]
            connection["target"] = connections_vertices[connection["target"]]

        response["connections"] = connections

        response = self.get_root_node_connections(response, root_entities, connections_vertices, query_string)

        response = self.get_stripped_nodes_and_depth(response)

        return response

    def run_query(self, client_id, query_string, dynamic=False):
        """
        Use the explore method to create a graph object.
        :param client_id:
        :param query_string:
        :param dynamic:
        :return:
        """
        try:

            if dynamic:
                response = self.get_dynamic_cluster_response(client_id, query_string)
            else:
                response = self.get_cluster_response(client_id, query_string)

            if response:
                # TODO: Refactor to non-hardcoded method
                response['cluster_id'] = IDGenerator.generate_cluster_id(client_id=client_id)
                response["date"] = self.get_datetime_key()
                response.pop("took")
                response.pop("timed_out")
                response.pop("failures")

                if response['connections']:
                    clust = 1
                    for item in response['connections']:
                        item["confirmation_status"] = 0
                        item["connection_id"] = response['cluster_id'] + "-" + clust.__str__()
                        clust += 1

                return response

        except Exception as e:
            logger.error('Exception `%s` running query', e, exc_info=True)
            print(e)

    def save_to_most_recent(self, cluster):
        """
        Save to most recent will use the cluster_id to update in place the data when a new cluster has been generated.

        This will allow for a quick scan across all clusters and return the most up to date information.

        :param cluster: <type 'dict'>
        :return:
        """
        cluster["date"] = self.get_datetime_key()

        self.client.index(
            index=self.cluster_index,
            doc_type=self.cluster_most_recent,
            id=cluster['cluster_id'],
            body=cluster
        )

        return

    def save_to_timeline(self, cluster):
        """
        Save to timeline will use an autogenerated id and write to new document each time.

        This will prevent individual documents becoming too large due to long timelines.

        A query on the cluster_id field within the _source will be required to retrieve all information relating to a
        cluster. This will be less efficient than a direct lookup on a cluster id but the index should be more stable
        due to the smaller document size.

        :param cluster: <type 'dict'>
        :return:
        """
        cluster["date"] = self.get_datetime_key()

        self.client.index(
            index=self.cluster_index,
            doc_type=self.cluster_timeline,
            body=cluster
        )

        return

    def set_mappings(self):
        """
        Set the mappings for most_recent and timeline doc types
        :return:
        """
        body = ElasticSearchClient.get_cluster_mappings()

        self.indices_client.put_mapping(index=self.cluster_index, doc_type=self.cluster_most_recent, body=body)

        self.indices_client.put_mapping(index=self.cluster_index, doc_type=self.cluster_timeline, body=body)

        return

    def set_raw_index_and_mappings(self):
        """
        Set raw index and mappings
        :return:
        """
        if not self.indices_client.exists(index=self.raw_documents_index):
            self.indices_client.create(index=self.raw_documents_index)
        else:
            pass

        if not self.indices_client.exists_type(index=self.raw_documents_index, doc_type="raw"):
            mappings = ElasticSearchClient.get_raw_document_mappings()
            self.indices_client.put_mapping(index=self.raw_documents_index, doc_type="raw", body=mappings)
        else:
            pass

        return

    def create_index(self):
        """
        Create cluster index
        :return:
        """
        try:
            self.indices_client.create(index=self.cluster_index)
        except (RequestError, TransportError) as e:
            print 'create_index problem: %s' % e

    def set_clustering_detail_index_and_mappings(self):
        """
        Set clustering_detail index and mappings
        :return:
        """
        if not self.read_client.indices.exists(index=u"clustering_detail"):
            self.read_client.indices.create(index=u"clustering_detail")
        else:
            pass

        if not self.read_client.indices.exists_type(index=u"clustering_detail", doc_type=u"clustering"):
            mappings = ElasticSearchClient.get_clustering_detail_mappings()
            self.read_client.indices.put_mapping(index=u"clustering_detail", doc_type=u"clustering", body=mappings)
        else:
            pass

        return

    def set_clustering_ui_index_and_mappings(self):
        """
        Set clustering_ui index and mappings
        :return:
        """

        if not self.client.indices.exists(index=self.cluster_index):
            self.client.indices.create(index=self.cluster_index)
        else:
            pass

        if not self.client.indices.exists_type(index=self.cluster_index, doc_type=self.cluster_most_recent):
            mappings = ElasticSearchClient.get_cluster_mappings()
            self.client.indices.put_mapping(index=self.cluster_index, doc_type=self.cluster_most_recent, body=mappings)
        else:
            pass

        return

    def delete_most_recent(self, cluster_id):
        """
        Delete the data present in most_recent doc_type with _id == cluster_id. _id will be a straight lookup
        :return:
        """
        try:
            self.client.delete(
                index=self.cluster_index,
                doc_type=self.cluster_most_recent,
                id=cluster_id
            )

        except TransportError as e:
            logger.error('TransportError `%s` deleting most recent', e, exc_info=True)
            print(e)

        except Exception as e:
            logger.error('Exception `%s` deleting most recent', e, exc_info=True)
            print(e)

        return

    def delete_timeline(self, cluster_id):
        """
        Delete the data present in timeline doc_type with cluster_id == cluster_id. cluster_id will be a query
        :return:
        """
        try:
            query = {
                "query": {
                    "bool": {
                        "must": {
                            "term": {
                                "cluster_id": cluster_id
                            }
                        }
                    }
                }
            }

            self.client.delete_by_query(
                index=self.cluster_index,
                doc_type=self.cluster_timeline,
                body=query
            )

        except TransportError as e:
            logger.error('TransportError `%s` deleting timeline', e, exc_info=True)
            print(e)

        except Exception as e:
            logger.error('Exception `%s` deleting timeline', e, exc_info=True)
            print(e)

        return

    def delete_cluster(self, cluster_id):
        """
        Delete the data present in most_recent doc_type with _id == cluster_id. _id will be a straight lookup
        Delete the data present in timeline doc_type with cluster_id == cluster_id. cluster_id will be a query
        :return:
        """
        if cluster_id:

            self.delete_most_recent(cluster_id=cluster_id)
            self.delete_timeline(cluster_id=cluster_id)

            return 0
        else:
            raise ValueError("cluster_id required for cluster deletion")

    def _extract_entity_from_query(self, query_string):
        """
        system created clusters stem from a start point specified in the form:
            entity_type:entity_value
        eg) host_ip:152.65.254.1
            domain:\"watch-ukraine.com.ua\"
            email:ebay@infiniteshopping.com

        this function breaks the query down into it's constituant parts: entity_type:entity_value
        """
        try:
            split = query_string.split(':')
            if len(split) > 1:
                entity_type = unicode(split[0])
                entity_value = unicode(':'.join(split[1:]))

                if '"' in entity_value:
                    entity_value = entity_value.replace('"', '')

                entity_value = entity_value.strip()

                return entity_type, entity_value
        except Exception, e:
            logger.error('error extracting entity from query %s - %s', query_string, e)
        return None, None   # if it isn't in our format then don't return any result

    def extract_entities_list_from_response(self, response):
        """
        Clustering using the spidering query will require the entities from the previous repsonse object to be passed
        into the new spider query
        :param response:
        :return:
        """
        entities_list = {}

        if "vertices" in response.keys():
            if response["vertices"]:
                for entity in response["vertices"]:

                    if entity["field"] in entities_list:
                        entities_list[entity["field"]].append({"term": entity["term"], "boost": entity["weight"]})

                    else:
                        entities_list[entity["field"]] = [{"term": entity["term"], "boost": entity["weight"]}]

        return entities_list

    def get_filtered_cluster(self, cluster, search_depth):
        """
        Remove vertices above search depth
        Get reindexed vertices values based on new ordering

        Remove connections relating to removed vertices
        Remap connections based on reindexed values

        :param cluster:
        :param search_depth:
        :return:
        """
        if "vertices" in cluster.keys():
            vertices = cluster.pop("vertices")
            cluster["vertices"] = []
        else:
            vertices = []

        if "connections" in cluster.keys():
            connections = cluster.pop("connections")
            cluster["connections"] = []
        else:
            connections = []

        indicies = {}
        index_ = 0

        if cluster:
            if vertices:
                for item in enumerate(vertices):
                    if item[1][u"depth"] or item[1][u"depth"] == 0:
                        if item[1]["depth"] <= search_depth:
                            cluster["vertices"].append(item[1])
                            indicies[item[0]] = index_
                            index_ += 1
                        else:
                            pass
                    else:
                        pass

            if connections:
                for item in connections:
                    if item["source"] in indicies.keys() and item["target"] in indicies.keys():

                        item["source"] = indicies[item["source"]]
                        item["target"] = indicies[item["target"]]

                        cluster["connections"].append(item)
                    else:
                        pass

        return cluster

    def get_cluster_by_cluster_id(self, cluster_id, search_depth=5):
        """
        Use Case #1.2 - An analyst wants the ability to get a cluster and update it

        :param search_depth: <type 'int'>
        :param cluster_id: <type 'str'>
        :return:
        """
        if cluster_id:
            try:
                cluster = self.client.get(index=self.cluster_index, doc_type=self.cluster_most_recent, id=cluster_id)

                if cluster:
                    try:

                        cluster = self.get_filtered_cluster(cluster["_source"], search_depth=search_depth)

                        return cluster
                    except Exception as e:
                        logger.error('Exception `%s` getting cluster by cluster id', e, exc_info=True)
                        print(e)
                else:
                    print("Could not find cluster {}".format(cluster_id))

            except TransportError as e:
                logger.error('TransportError `%s` getting graph client', e, exc_info=True)
                print(e)

            except Exception as e:
                logger.error('Exception `%s` getting graph client', e, exc_info=True)
                print(e)

    def _change_cluster_id(self, cluster, cluster_id):
        """
        change the cluster id used by a cluster

        probably should change the cluster code to take a cluster id but doing this here as it is
        ten minutes to midnight.
        """
        current_cluster_id = cluster['cluster_id']
        cluster['cluster_id'] = cluster_id

        for connection in cluster['connections']:
            if connection['connection_id'].startswith(current_cluster_id):
                connection['connection_id'] = connection['connection_id'].replace(current_cluster_id, cluster_id)
        return cluster

    def save_cluster_to_index(self, cluster):
        """
        Use Case #1.3 - An analyst wants the ability to save down the dynamically defined cluster

        The cluster will be exposed to the most_recent and timeline doctypes

        The most recent data will overwrite any existing data while the timeline data will append to the _source

        :param cluster: <type 'dict'>
        :return:

        modified to now look up and see if a previous cluster exists for the same query and if it does to save this data over the top.
        """
        if cluster:
            previous_cluster_id = self.get_cluster_id_for_query(client_id=cluster['client_id'], query_string=cluster['query'])
            if previous_cluster_id:
                cluster = self._change_cluster_id(cluster, previous_cluster_id)
            self.save_to_most_recent(cluster)

            # self.save_to_timeline(cluster)

        return

    def get_all_clusters(self, client_id, search_term=None, filters=None, sort="num_issues"):
        """
        Use Case #2.1 - An analyst wants the most recent data available on all clusters.

        *** Read from most recent doctype

        Filters/Parameters need to be dynamically set
        get_all_clusters call to endpoint clusters
        cluster ids need to have been assigned
        cluster endpoint needs to have been populated

        :param client_id:
        :param search_term:
        :param filters:
        :param sort:
        :return:
        """
        self.index = client_id

        if filters:
            self.get_filters(**filters)
        else:
            self.get_filters()

        if not search_term:
            query = {
                "_source": {
                    "excludes": ["connections", "vertices"]
                },
                "query": {
                    "bool": {
                        "must": {
                            "term": {
                                "client_id": client_id
                            }
                        },
                        "filter": self.filters
                    }
                },
                "size": 25,
                "sort": [
                    {sort: "desc"}
                ]
            }
        else:
            query = {
                "_source": {
                    "excludes": ["connections", "vertices"]
                },
                "query": {
                    "query_string": {
                        "query": "\"{}\" AND client_id: {}".format(search_term, client_id)
                    }
                },
                "size": 25,
                "sort": [
                    {sort: "desc"}
                ]
            }

        try:

            clusters = self.client.search(
                index=self.cluster_index,
                doc_type=self.cluster_most_recent,
                body=query
            )

            for cluster in clusters['hits']['hits']:
                yield cluster["_source"]

        except TransportError as e:
            logger.error('TransportError `%s` getting all clusters', e, exc_info=True)
            print(e)

        except Exception as e:
            logger.error('Exception `%s` getting all clusters', e, exc_info=True)
            print(e)

    def get_cluster_timeline(self, cluster_id, start_date=None, end_date=None):
        """
        Use Case #3.1 - An analyst wants the ability to view the entire cluster over a time period.

        *** Read from timeline doctype

        Filters/Parameters need to be dynamically set
        client.search w/ a cluster id and return all instances of that cluster

        User provided start_date and/or end_date will be appended to query if exists
        start_date/end_date need to be in the "format": "yyyy-MM-dd HH:mm:ss"

        :param cluster_id: <type 'str'>
        :param start_date: <type 'str'> "yyyy-MM-dd HH:mm:ss"
        :param end_date: <type 'str'> "yyyy-MM-dd HH:mm:ss"
        :return:
        """
        try:

            dates = {"format": "yyyy-MM-dd HH:mm:ss"}

            if start_date:
                dates["gte"] = start_date

            if end_date:
                dates["lt"] = end_date

            query = {
                "query": {
                    "bool": {
                        "must": {
                            "term": {
                                "cluster_id": cluster_id
                            }
                        },
                        "filter": {
                            "range": {
                                "date": dates
                            }
                        }
                    }
                }
            }

            cluster = self.client.search(index=self.cluster_index, doc_type=self.cluster_timeline, body=query)

            if cluster:
                return cluster
            else:
                pass

        except TransportError as e:
            logger.error('TransportError `%s` getting cluster timeline', e, exc_info=True)
            print(e)

        except Exception as e:
            logger.error('Exception `%s` getting cluster timeline', e, exc_info=True)
            print(e)

    def get_document_terms(self, cluster):
        """
        Pass in a cluster and get all vetex terms
        :param cluster:
        :return:
        """
        document_terms = defaultdict(list)

        if cluster:
            if cluster['vertices']:
                if isinstance(cluster['vertices'], list):
                    for element in cluster['vertices']:
                        document_terms[element['field']].append(element['term'])

        return document_terms

    def get_raw_documents(self, cluster, client_id):
        """
        Get the raw documents retrieved from infringement index that the cluster is being created from.

        If entity is supplied, only documents matching that entity are returned.
        TODO: use entity_type to optimise entity filter
        :param entity_type: <type 'string'>
        :param entity_name: <type 'string'>
        :return:
        """
        document_terms = self.get_document_terms(cluster)

        should_list = []

        if document_terms:
            for key, value in document_terms.items():
                if isinstance(value, list):
                    should_list.append({"terms": {key: value}})
                else:
                    should_list.append({"term": {key: value}})

            query = {
                "query": {
                    "constant_score": {
                        "filter": {
                            "bool": {
                                "minimum_should_match": 1,
                                "should": should_list
                            }
                        }
                    }
                }
            }

            raw_documents = scan(self.client, query=query, scroll='1m', index=client_id)

            for doc in raw_documents:
                if doc:
                    yield doc

    def get_raw_documents_from_connections(self, client_id, source_type, source_value, target_type=None, target_value=None):
        """
        Explore the raw documents that form the connections
        :return:
        """
        if not target_type and not target_value:
            query = {
                "query": {
                    "query_string": {
                        "query": "{}: \"{}\"".format(source_type, source_value)
                    }
                },
                "size": 10000
            }
        else:
            query = {
                "query": {
                    "query_string": {
                        "query": "{}: \"{}\" AND {}: \"{}\"".format(source_type, source_value, target_type, target_value)
                    }
                },
                "size": 10000
            }

        raw_docs = self.client.search(
            index=client_id,
            body=query
        )

        for hit in raw_docs['hits']['hits']:
            yield hit

    @staticmethod
    def get_areas_count(metrics, element):
        """

        :param metrics:
        :param element:
        :return:
        """
        metric_keys = ["price", "quantity_in_stock", "quantity_sold", "potential_sales_total", "sales_total"]

        try:
            if element["_type"]:
                if element["_type"] in metrics["areas"]:
                    metrics["area_metrics"][element["_type"]]["num_issues"] += 1

                    for metric_key in metric_keys:
                        if metric_key in element["_source"].keys():
                            if element["_source"][metric_key]:
                                metrics["area_metrics"][element["_type"]][metric_key] += float(element["_source"][metric_key])

                else:
                    metrics["areas"].append(element["_type"])

                    metrics["area_metrics"][element["_type"]] = {
                        "num_issues": 0,
                        "price": 0,
                        "quantity_in_stock": 0,
                        "quantity_sold": 0,
                        "potential_sales_total": 0,
                        "sales_total": 0
                    }

                    metrics["area_metrics"][element["_type"]]["num_issues"] += 1

                    for metric_key in metric_keys:
                        if metric_key in element["_source"].keys():
                            if element["_source"][metric_key]:
                                metrics["area_metrics"][element["_type"]][metric_key] += float(element["_source"][metric_key])

        except Exception as e:
            logger.error('Exception `%s` getting area count', e, exc_info=True)
            print(e)

        return metrics

    @staticmethod
    def get_risk_count(metrics, element):
        """

        :param metrics:
        :param element:
        :return:
        """
        try:
            if "risk" in element["_source"].keys():
                if element["_source"]["risk"]:
                    if element["_source"]["risk"] in metrics["risk"]:
                        metrics["risk_metrics"][element["_source"]["risk"]]["num_issues"] += 1

                    else:
                        metrics["risk"].append(element["_source"]["risk"])

                        metrics["risk_metrics"][element["_source"]["risk"]] = {
                            "num_issues": 0
                        }

                        metrics["risk_metrics"][element["_source"]["risk"]]["num_issues"] += 1

        except Exception as e:
            logger.error('Exception `%s` getting risk count', e, exc_info=True)
            print(e)

        return metrics

    @staticmethod
    def get_status_count(metrics, element):
        """

        :param metrics:
        :param element:
        :return:
        """
        try:
            if "status" in element["_source"].keys():
                if element["_source"]["status"]:
                    if element["_source"]["status"] in metrics["status"]:
                        metrics["status_metrics"][element["_source"]["status"]]["num_issues"] += 1

                    else:
                        metrics["status"].append(element["_source"]["status"])

                        metrics["status_metrics"][element["_source"]["status"]] = {
                            "num_issues": 0
                        }

                        metrics["status_metrics"][element["_source"]["status"]]["num_issues"] += 1

        except Exception as e:
            logger.error('Exception `%s` getting status count', e, exc_info=True)
            print(e)

        return metrics

    def get_metric_counts(self, brands, metrics, element):
        """
        :param element
        :param metrics:
        :return:
        """
        try:

            for metric_key in metrics.keys():
                if metric_key == "brands":
                    if "brand" in element["_source"].keys():
                        if isinstance(element["_source"]["brand"], list):
                            for key in element["_source"]["brand"]:
                                brands.append(key)
                        elif isinstance(element["_source"]["brand"], (str, unicode)):
                            brands.append(element["_source"]["brand"])
                        else:
                            pass

                elif metric_key in ["price", "quantity_in_stock", "quantity_sold", "potential_sales_total",
                                    "sales_total"]:
                    if metric_key in element["_source"].keys():
                        if element["_source"][metric_key]:
                            metrics[metric_key] += float(element["_source"][metric_key])
                    else:
                        pass
                else:
                    pass

            metrics['brands'] = list(set(brands))

        except Exception as e:
            logger.error('Exception `%s` getting metric counts', e, exc_info=True)
            print(e)

        return metrics

    @staticmethod
    def get_empty_metrics():
        metrics = {
            "num_issues": 0,
            "price": 0,
            "quantity_in_stock": 0,
            "quantity_sold": 0,
            "potential_sales_total": 0,
            "sales_total": 0,
            "brands": [],
            "areas": [],
            "risk": [],
            "status": [],
            "area_metrics": {},
            "risk_metrics": {},
            "status_metrics": {}
        }
        return metrics

    def get_quantitative_metrics(self, cluster, client_id):
        """
        num_issues, price, quantity_in_stock, quantity_sold, potential_sales_total, sales_total, brands
        :return:
        """
        metrics = {
            "num_issues": 0,
            "price": 0,
            "quantity_in_stock": 0,
            "quantity_sold": 0,
            "potential_sales_total": 0,
            "sales_total": 0,
            "brands": [],
            "areas": [],
            "risk": [],
            "status": [],
            "area_metrics": {},
            "risk_metrics": {},
            "status_metrics": {}
        }

        brands = []

        for element in self.get_raw_documents(cluster, client_id):
            try:
                if isinstance(element, dict):
                    metrics["num_issues"] += 1

                    metrics = self.get_areas_count(metrics, element)

                    metrics = self.get_risk_count(metrics, element)

                    metrics = self.get_status_count(metrics, element)

                    metrics = self.get_metric_counts(brands, metrics, element)

            except Exception as e:
                logger.error('Exception `%s` getting quantitative metrics', e, exc_info=True)
                print(e)

        return metrics

    def get_filter_metrics(self, filters, cluster):
        """

        :param filters:
        :param cluster:
        :return:
        """
        if filters:
            for key, value in filters.items():
                if isinstance(value, dict):
                    for key1, value1 in value.items():
                        cluster[key + '_' + key1] = value1
                else:
                    cluster[key] = value

            return cluster
        else:
            return cluster

    def get_clustering_charts(self, client_id, filters=None, sort="num_issues"):
        """
        Aggregation needed to get the sum of the number of issues by infringement _type
        :param filters:
        :param sort:
        :return:
        """
        if filters:
            self.get_filters(**filters)
        else:
            self.get_filters()

        query = {
            'aggregations': {
                'breakdown_by_brand': {
                    'aggregations': {
                        'results': {
                            'terms': {
                                'field': 'cluster_id',
                                'order': {
                                    '_count': 'desc'
                                }
                            }
                        }
                    },
                    'terms': {
                        'field': 'brands.keyword', 'size': 15
                    }
                }
            },
            'query': {
                'bool': {
                    "must": [
                        {
                            'query_string': {
                                'default_field': 'client_id', 'query': client_id
                            }
                        }
                    ],
                    "filter": self.filters
                }
            },
            "sort": [
                {sort: "desc"}
            ]
        }

        agg = self.client.search(
            index=self.cluster_index,
            doc_type=self.cluster_most_recent,
            body=query
        )
        return agg

    def get_top_clusters_query(self, client_id, filters=None, sort="num_issues"):
        """

        :return:
        """
        if filters:
            self.get_filters(**filters)
        else:
            self.get_filters()

        query = {
            "query": {
                "bool": {
                    "must": {
                        "term": {
                            "client_id": client_id
                        }
                    },
                    "filter": self.filters
                }
            },
            "size": 15,
            "sort": [
                {sort: "desc"}
            ]
        }

        results = self.client.search(
            index=self.cluster_index,
            doc_type=self.cluster_most_recent,
            body=query
        )

        return results

    @staticmethod
    def update_connection(cluster, conn_id, new_conn_status):
        """
        Set confirmation_status to new_conn_status
        :param cluster:
        :param conn_id:
        :param new_conn_status:
        :return:
        """
        if "connections" in cluster.keys():
            if cluster["connections"]:
                for element in cluster["connections"]:
                    if element["connection_id"] == conn_id:
                        element["confirmation_status"] = new_conn_status

        return cluster

    def update_cluster_connection(self, cluster_id, conn_id, new_conn_status):
        """
        User defined ability to update a connection status to new_conn_status

        :param cluster_id:
        :param conn_id:
        :param new_conn_status:
        :return:
        """
        cluster = self.get_cluster_by_cluster_id(cluster_id=cluster_id)

        cluster = self.update_connection(cluster, conn_id, new_conn_status)

        self.save_cluster_to_index(cluster)

        return 0

    def create_cluster(self, client_id, infringement_doctype, filters, search_term, dynamic=False, run_metrics=True, sample_size=None):
        """
        Use Case #1.1 - An analyst wants to find a cluster given a certain piece of information.

        Filters/Parameters need to be dynamically set
        Cluster needs to be generated by explore function
        User needs the ability to add/refine cluster dynamically
        Cluster needs to be saved down to cluster endpoint
        Cluster id needs to be generated

        Index will be Client ID. Should be defined at initiation

        run_metrics should be set to True unless query_string is vague and will return massive amount of matches
        E.g. "phone_num_in_post:*1*" will have 000's of entities matching it

        :param infringement_doctype: <type 'list'> ['marketplace_infringement', 'chorus', etc.]
        :param filters: <type 'dict'>
        :param query_string: <type 'string'>
        :param dynamic: <type 'boolean'>
        :param run_metrics: <type 'boolean'>
        :param sample_size: <type 'int'>

        :return:
        """
        self.index = client_id

        if infringement_doctype:
            self.infringement_doctype = infringement_doctype

        if filters:
            self.get_filters(**filters)

        if search_term:
            self.search_term = search_term

        if sample_size:
            self.sample_size = sample_size

        # Check if cluster on search_term already exists. Do not create new cluster
        entity_type, entity_value = self._extract_entity_from_query(search_term)
        clusters = [c["cluster_id"] for c in self.get_all_clusters(client_id, search_term=entity_value)]

        if not clusters:

            cluster = self.run_query(client_id=client_id, query_string=search_term, dynamic=dynamic)

            if cluster:
                try:

                    # store the query and it's breakdown - this helps with
                    # re-running this later when updates occur.
                    cluster["query"] = search_term
                    entity_type, entity_value = self._extract_entity_from_query(search_term)
                    cluster["query_type"] = entity_type
                    cluster["query_value"] = entity_value
                    # cluster["query_value"] = '"{}"'.format(entity_value)

                    # Default to True.
                    if run_metrics:
                        metrics = self.get_quantitative_metrics(cluster, client_id)

                        # Add metric values to cluster
                        for key, value in metrics.items():
                            cluster[key] = value
                    else:
                        metrics = self.get_empty_metrics()
                        # Add metric values to cluster
                        for key, value in metrics.items():
                            cluster[key] = value

                    # Add client_id to cluster
                    cluster["client_id"] = client_id

                    if cluster["vertices"]:
                        for vertex in cluster["vertices"]:
                            # TODO: Rearrange to calculate count
                            vertex["value"] = 1

                    return cluster
                except Exception as e:
                    logger.error('Exception `%s` creating cluster', e, exc_info=True)
                    print(e)
            else:
                logger.info('No cluster created for search_term %s', search_term)
        else:
            logger.info('Entity already exists in cluster(s): {}'.format(clusters))
            print("Entity already exists in cluster(s): {}".format(clusters))

    def save_to_raw_documents(self, cluster, entity_type=None, entity_name=None):
        """
        Save raw documents for a cluster or entity in the cluster in the es 1.6 index.
        :param entity_type: <type 'string'>
        :param entity_name: <type 'string'>
        :return:
        """
        for element in self.get_raw_documents_from_connections(client_id=cluster["client_id"], source_type=entity_type, source_value=entity_name):
            element["_source"]["cluster_name"] = cluster["cluster_id"]

            keys = ["price", "company_registration", "sales_total", "quantity_in_stock",
                    "quantity_sold", "potential_sales_total", "authorized_ids",
                    "user_profile_url", "site_display_name", "representative", "member_since", "model_number",
                    "company", "display_name", "keywords", "marketplace", "seller_id",
                    "user_id", "user_name"]

            for key in keys:
                if key in element["_source"].keys():
                    element["_source"].pop(key)

            if 'domain' in element["_source"] and element["_source"]["domain"]:
                element["_source"]["domain_marketplace_platform"] = element["_source"]["domain"]
                # If domain, person/keyword column is keyword
                if 'keywords' in element["_source"] and element["_source"]["keywords"]:
                    element["_source"]["person_keyword"] = element["_source"]["keywords"]
            else:
                if 'site_display_name' in element["_source"] and element["_source"]["site_display_name"]:
                    element["_source"]["domain_marketplace_platform"] = element["_source"]["site_display_name"]
                if 'seller_id' in element["_source"] and element["_source"]["seller_id"]:
                    element["_source"]["person_keyword"] = element["_source"]["seller_id"]
                elif 'user' in element["_source"] and element["_source"]["user"]:
                    element["_source"]["person_keyword"] = element["_source"]["user"]

            if 'detection_date' in element["_source"] and element["_source"]["detection_date"]:
                element["_source"]["item_detection_date"] = element["_source"]["detection_date"]

            # Write to old ES Index 1.6
            self.read_client.index(
                index="analyse_uicluster_area_titan_index",
                doc_type="analyse_uicluster_area_titan",
                id=element["_id"],
                body=element["_source"],
                refresh=u'true'
            )

    def delete_cluster_by_client_id(self, client_id):
        """
        Delete the data present in ui_cluster with client_id == client_id.
        :return:
        """
        try:
            query = {
                "query": {
                    "bool": {
                        "must": {
                            "term": {
                                "client_id": client_id
                            }
                        }
                    }
                }
            }

            self.client.delete_by_query(
                index=self.cluster_index,
                doc_type=self.cluster_most_recent,
                body=query
            )

        except TransportError as e:
            logger.error('TransportError `%s` deleting cluster by client id', e, exc_info=True)
            print(e)

        except Exception as e:
            logger.error('Exception `%s` deleting cluster by client id', e, exc_info=True)
            print(e)

        return

    def main(self, client_id, search_term=None, filters=None, sort="num_issues", search_depth=5, infringement_doctype=None):
        """
        User enters in a search term in the UI search bar function.

        Backend logic determines if cluster_id, value in vertex or a new cluster

        if search_term == regex(clustering_id):
            list(self.get_cluster_by_cluster_id)
        else
            self.get_all_clusters(search_term)

            if create_dynamic:
                self.create_cluster(search_term)

        :return:
        """
        clusters = []

        if not search_term:
            for cluster in self.get_all_clusters(client_id=client_id, search_term=search_term, filters=filters, sort=sort):
                clusters.append(cluster)

            return clusters
        else:
            if area_from_fid(search_term) == 'clustering':
                clusters.append(self.get_cluster_by_cluster_id(cluster_id=search_term, search_depth=search_depth))

                return clusters
            else:

                for cluster in self.get_all_clusters(client_id=client_id, search_term=search_term, filters=filters, sort=sort):
                    cluster["dynamic"] = False
                    clusters.append(cluster)

                if use_dynamic:
                    search_term = "\"{}\"".format(search_term)
                    cluster = self.create_cluster(
                        client_id=client_id,
                        infringement_doctype=infringement_doctype,
                        filters=filters,
                        search_term=search_term,
                        dynamic=True,
                        sample_size=1000
                    )

                    cluster = self.get_filtered_cluster(cluster, search_depth=search_depth)

                    if cluster:
                        cluster["dynamic"] = True
                        clusters.append(cluster)
                    else:
                        logger.warn("No dynamic cluster generated for `%s`, client=%s", search_term, client_id)

                return clusters
