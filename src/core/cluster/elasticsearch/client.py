from dependencies.elasticsearch_5_4_0 import Elasticsearch as Elasticsearch_5_4_0
from dependencies.elasticsearch_5_4_0.client.indices import IndicesClient as IndicesClient_5_4_0

from core.framework.elasticsearch.es_utils import get_elasticsearch
from elasticsearch.client.indices import IndicesClient
from elasticsearch.exceptions import ElasticsearchException

from django.conf import settings

CLUSTERING_ES_PASSWORD = getattr(settings, 'CLUSTERING_ES_PASSWORD', False)
CLUSTERING_ES_USER = getattr(settings, 'CLUSTERING_ES_USERNAME', False)
CLUSTERING_ES_HOST = getattr(settings, 'CLUSTERING_ES_HOST', False)
CLUSTERING_ES_PORT = getattr(settings, 'CLUSTERING_ES_PORT', False)


class ScanError(ElasticsearchException):
    pass


class ElasticSearchClient(object):
    def __init__(self):
        self.read_client = self.get_read_client()
        self.write_client = self.get_write_client()
        self.read_indices_client = self.get_read_indices_client()
        self.indices_client = self.get_indices_client()

    @classmethod
    def get_config(cls):

        _config = {
            "user": CLUSTERING_ES_USER,
            "password": CLUSTERING_ES_PASSWORD
        }

        return _config

    @classmethod
    def get_read_client(cls):
        """
        ES v1.6.4 client
        :return: <type 'elasticsearch.client.Elasticsearch'>
        """
        return get_elasticsearch()

    @classmethod
    def get_write_client(cls):
        """
        ES v5.6.4 client
        :return: <type 'elasticsearch.client.Elasticsearch'>
        """
        node = [{"host": CLUSTERING_ES_HOST, "port": CLUSTERING_ES_PORT}]

        config = ElasticSearchClient.get_config()
        http_auth = (config['user'], config['password'])

        write_client = Elasticsearch_5_4_0(node, http_auth=http_auth)

        return write_client

    @classmethod
    def get_read_indices_client(cls):
        """
        ES v1.6.4 indices client
        :return: <type 'elasticsearch.client.indices.IndicesClient'>
        """
        indices_client = IndicesClient(ElasticSearchClient.get_read_client())
        return indices_client

    @classmethod
    def get_indices_client(cls):
        """
        ES v5.6.4 client
        :return: <type 'elasticsearch.client.indices.IndicesClient'>
        """
        indices_client = IndicesClient_5_4_0(ElasticSearchClient.get_write_client())
        return indices_client

    def create_elasticsearch_index(self, output):
        """
        ES v5.6.4 client. Create index
        :param output:
        :return:
        """
        try:
            self.indices_client.create(index=output["_index"])
            print("Created Index {}".format(output["_index"]))
            return
        except Exception as e:
            print("Could not create index. Error {}".format(e))

    def create_elasticsearch_mappings(self, output):
        """
        ES v5.6.4 client. Create mapping for each doc type
        :param output:
        :return:
        """
        try:
            _mappings = []

            for key in output["_source"]:
                _mappings.append((key, {
                    "type": "keyword"
                }))

            _mappings = dict(_mappings)

            try:
                pass
                # _mappings["price"] = {"type": "double"}
            except Exception as e:
                print(e)

            _mappings = {output["_type"]: {"properties": _mappings}}

            self.indices_client.put_mapping(index=output["_index"], doc_type=output["_type"], body=_mappings)
            print("Created mapping {}".format(output["_type"]))
            return
        except Exception as e:
            print("Could not create mappings. Error {}".format(e))

    def create_elasticsearch_setup(self, output):
        """
        Create ES index or type mappings if none exist
        :return:
        """
        if not self.indices_client.exists(output["_index"]):
            self.create_elasticsearch_index(output)
        else:
            pass

        if not self.indices_client.exists_type(index=output["_index"], doc_type=output["_type"]):
            self.create_elasticsearch_mappings(output)
        else:
            pass

    @classmethod
    def get_cluster_mappings(cls):
        _mappings = {
            "properties": {
                "vertices": {
                    "type": "nested"
                },
                "connections": {
                    "type": "nested"
                },
                "cluster_id": {
                    "type": "keyword"
                },
                "date": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss"
                },
                "num_issues": {
                    "type": "long"
                },
                "potential_sales_total": {
                    "type": "long"
                },
                "price": {
                    "type": "float"
                },
                "quantity_in_stock": {
                    "type": "long"
                },
                "quantity_sold": {
                    "type": "long"
                },
                "sales_total": {
                    "type": "long"
                },
                "query": {
                    "type": "keyword"
                },
                "query_type": {
                    "type": "keyword"
                },
                "query_value": {
                    "type": "keyword"
                }
            }
        }

        return _mappings

    @classmethod
    def get_raw_document_mappings(cls):
        _mappings = {
            "properties": {
                "area": {
                    "type": "keyword"
                },
                "detection_date": {
                    "type": "date",
                    "format": "yyyy-MM-dd'T'HH:mm:ss"
                },
                "client_id": {
                    "type": "keyword"
                },
                "collection": {
                    "type": "keyword"
                },
                "keywords": {
                    "type": "keyword"
                },
                "marketplace": {
                    "type": "keyword"
                },
                "cluster_name": {
                    "type": "keyword"
                },
                "cluster_id": {
                    "type": "keyword"
                },
                "brand": {
                    "type": "keyword"
                },
                "username": {
                    "type": "keyword"
                },
                "email": {
                    "type": "keyword"
                },
                "phone": {
                    "type": "keyword"
                },
                "address": {
                    "type": "keyword"
                },
                "wechat": {
                    "type": "keyword"
                },
                "qq": {
                    "type": "keyword"
                },
                "whatsapp": {
                    "type": "keyword"
                },
                "twitter": {
                    "type": "keyword"
                },
                "pinterest": {
                    "type": "keyword"
                },
                "flickr": {
                    "type": "keyword"
                },
                "googleplus": {
                    "type": "keyword"
                },
                "youtube": {
                    "type": "keyword"
                },
                "instagram": {
                    "type": "keyword"
                },
                "renren": {
                    "type": "keyword"
                },
                "weibo": {
                    "type": "keyword"
                },
                "host_ip": {
                    "type": "keyword"
                },
                "domain": {
                    "type": "keyword"
                },
                "bank_account": {
                    "type": "keyword"
                }
            }
        }

        return _mappings

    @classmethod
    def get_ui_mappings(cls, _type):
        """
        Adjust doc_type naming convention for consistency with UI objects
        :param mapping_id:
        :return:
        """
        ui_mappings = {
            'analyse_uidomain': 'domain',
            'marketplace_infringement': 'marketplace',
            'collect_marketplace_infringement': 'marketplace',
            'socialmedia_infringement': 'socialmedia',
            'collect_socialmedia_infringement': 'socialmedia',
            'paidsearch_infringement': 'paidsearch',
            'collect_paidsearch_infringement': 'paidsearch',
            'appstore_infringement': 'appstore',
            'collect_appstore_infringement': 'appstore',
            'threedimensionalprinting_infringement': '3dprinting',
            'collect_threedimensionalprinting_infringement': '3dprinting',
        }

        try:
            return ui_mappings[_type]
        except Exception as e:
            print(e)

    @classmethod
    def get_type_mappings(cls, index):
        """
        Pass in the index and return the desired doc type
        :return:
        """
        mappings = {
            'analyse_uidomain_index': 'analyse_uidomain',
            'marketplace_infringement': 'marketplace_infringement',
            'socialmedia_infringement': 'socialmedia_infringement',
            'paidsearch_infringement': 'paidsearch_infringement',
            'appstore_infringement': 'appstore_infringement',
            'threedimensionalprinting_infringement': 'threedimensionalprinting_infringement',
        }

        try:
            return mappings[index]
        except Exception as e:
            print(e)

    @staticmethod
    def get_clustering_detail_mappings():
        """
        Set clustering_detail mappings for 1.6 index
        :return:
        """

        _mappings = {
            'properties': {
                'domain_fuzzy': {'type': 'string'},
                'domain': {
                    "type": "multi_field",
                    "fields": {
                        "domain_clean": {
                            "type": "string"
                        },
                        "raw": {
                            "type": "string",
                            "index": "not_analyzed",
                            "copy_to": "domain.raw"
                        }
                    }
                },
                'instagram': {'type': 'string'},
                'twitter': {'type': 'string'},
                'udrp_user': {'type': 'string'},
                'phone': {'type': 'string'},
                'admin_id': {'type': 'string'},
                'linked_domain': {'type': 'string'},
                'keywords': {'type': 'string'},
                'territory': {'type': 'string'},
                'item_detection_date': {'type': 'date', 'format': 'dateOptionalTime'},
                'registrant_id': {'type': 'string'},
                'id': {'type': 'string'},
                'weibo': {'type': 'string'},
                'listing_id': {'type': 'string'},
                'registrar_name': {'type': 'string'},
                'user_tags': {'type': 'string'},
                'area': {'type': 'string'},
                'intermediaries': {'type': 'string'},
                'cluster_name': {'type': 'string'},
                'app_title': {'type': 'string'},
                'whatsapp': {'type': 'string'},
                'flickr': {'type': 'string'},
                'host_ip_location': {'type': 'string'},
                'email': {'type': 'string'},
                'status': {'type': 'long'},
                'about_user': {'type': 'string'},
                'product': {'type': 'string'},
                'udrp_friendly_id': {'type': 'string'},
                'risk': {'type': 'string'},
                'detection_date': {'type': 'date', 'format': 'dateOptionalTime'},
                'renren': {'type': 'string'},
                'brand': {'type': 'string'},
                'host_ip': {'type': 'string'},
                'collection': {'type': 'string'},
                'source_url': {'type': 'string'},
                'person_keyword': {'type': 'string'},
                'registrar_phone': {'type': 'string'},
                'wechat': {'type': 'string'},
                'user': {'type': 'string'},
                'client_id': {'type': 'string'},
                'address': {'type': 'string'},
                'name_servers': {'type': 'string'},
                'domain_marketplace_platform': {'type': 'string'},
                'ip_address': {'type': 'string'},
                'bank_account': {'type': 'string'},
                'client_reference': {'type': 'string'},
                'qq': {'type': 'string'},
                'registrar_email': {'type': 'string'},
                'about': {'type': 'string'},
                'data_source': {'type': 'string'},
                'name': {'type': 'string'},
                'listing_title': {'type': 'string'},
                'display_url': {'type': 'string'},
                'url': {'type': 'string'},
                'googleplus': {'type': 'string'},
                'youtube': {'type': 'string'},
                'username': {'type': 'string'},
                'pinterest': {'type': 'string'},
                'product_id': {'type': 'string'},
                'image_url': {'type': 'string'},
                'title': {'type': 'string'}
            }
        }

        return _mappings
